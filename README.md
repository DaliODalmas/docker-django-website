# docker-django-website
to run this site,

cd to the folder that has the docker-compose.yml file and use the following commands

- docker-compose build
- docker-compose up

you can then access the site from your browser using the link

http://127.0.0.1:8000

